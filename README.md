# helix

https://docs.helix-editor.com/commands.html
[Keymap](https://docs.helix-editor.com/keymap.html)

## Installation LSP languages:  
[Language Server Configurations](https://github.com/helix-editor/helix/wiki/Language-Server-Configurations)

```rust
rustup component add rls rust-analyzer clippy rust-std
```

## LSP configuration:  
https://github.com/helix-editor/helix/blob/master/languages.toml

### Probleme:
Couleur dans connection SSH → dans la conf ajouter `true-color = true`  
Sort sur Linux/MacOS/PowerShell -> `:pipe sort`  

# THEMES:

kanagawa
monokai_pro_octagon

# COMMANDS

:o → open file in current view  
:n / :new → create buffer  
:vs <path> → vertical split  
:hs <path> → horizontal split  
:vnew → vertical split new empty  
:hnew → horizontal split new empty  
:bn / gn → next buffer  
:bp / gp → previous buffer  
:bc → buffer close  

# NORMAL MODE

f <letter> → selection text until the next letter (including it)  
t <letter> → selection text juste before the next letter  
Ctrl + i → jump forward  
Ctrl + o → jump Backward  
Ctrl + j -> move current line up  
Ctrl + k -> move current line down  
Ctrl + c -> Comment/Uncomment line  

# CHANGES MODE

A → insert end line  
I → insert start line  

Copy replace:  

e + y → copy  
e + shift R → paste  

# GO TO MODE

g → open goto mode  
g r → check all references  
gt → go top of the screen  
gc → go center of the screen  
gb → go bottom of the screen  
gh -> go start of the line  
gl -> go end of the line  

Navigate buffer (onglet)  
gn -> next buffer  
gp -> previous buffer  

# MATCHING MODE

mm → jump to matching brackets  
ms <quote/parentheses/brackets…> → add <xxx> around a word (you must select the word before)  
md <quote/parentheses/brackets> → remote <xxx> around a word (pre-selected before)  

# SPACE MODE

space → open space mode  
Space f → Open search file  
Space F → Search file in current directory  
Space s → Open symbols search  
Space d → diagnostic messages  
Space a → code actions  
space k → Documentation functions  
space r → rename variable (automatic changes where variable is used)  

# VISUAL MODE

v → switch to visual mode  

remplacer plusieurs éléments →  

1) select word  
2) `*`  
3) n to select all occurences  
4) i to switch to edit mode  
5) edit your word.  

# WINDOW MODE  
Ctrl + w  
v (Ctrl + v) -> vertical split  
s (Ctrl + s) -> horizontal split  
q (Ctrl + q) -> close current window
o → only keep current window, closing others  

# SEARCH MODE

/ → search for regex  
`*`→ search selected pattern  